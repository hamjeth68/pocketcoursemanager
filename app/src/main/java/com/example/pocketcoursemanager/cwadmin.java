package com.example.pocketcoursemanager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class cwadmin extends AppCompatActivity {
     EditText etleid, pltxtln,pltxtln2;
     Button btnupdate,btndelete,btnadd,btnread;
    //private DBhelper dBhelper;
    db stdb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cwadmin);
        etleid=(EditText)findViewById(R.id.etleid);
        pltxtln=(EditText)findViewById(R.id.pltxtln);
        pltxtln2=(EditText)findViewById(R.id.pltxtln2);

        getSupportActionBar().hide();

        stdb=new db(cwadmin.this);


        btnadd=(Button)findViewById(R.id.btnadd);
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(cwadmin.this, "Successfull", Toast.LENGTH_SHORT).show();
                String tvleid = etleid.getText().toString();
                String tvles = pltxtln.getText().toString();
                String tvact = pltxtln2.getText().toString();

                stdb.adduser(tvleid, tvles, tvact);

            }

            {
                btnupdate=(Button)findViewById(R.id.btnupdate);
                btnupdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(cwadmin.this, "Initializing Update", Toast.LENGTH_SHORT).show();


                    }

                    {
                        btndelete=(Button)findViewById(R.id.btndelete);
                        btndelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(cwadmin.this, "Deleting!", Toast.LENGTH_SHORT).show();

                            }

                            {
                                btnread=(Button)findViewById(R.id.btnread);
                                btnread.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Toast.makeText(cwadmin.this, "ready to read", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

            }
        }


