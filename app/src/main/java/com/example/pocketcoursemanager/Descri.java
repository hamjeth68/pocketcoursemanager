package com.example.pocketcoursemanager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

public class Descri extends AppCompatActivity {

    Button cds1,cds2,cds3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descri);

        cds1 = (Button) findViewById(R.id.cds1);
        cds2 = (Button) findViewById(R.id.cds2);
        cds3 = (Button) findViewById(R.id.cds3);

        getSupportActionBar().hide();

        cds1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Descri.this,description2.class);
                startActivity(i);

                cds2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent a = new Intent(Descri.this,description3.class);
                        startActivity(a);

                        cds3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent c = new Intent(Descri.this,description4.class);
                                startActivity(c);
                            }
                        });
                    }
                });
            }
        });
    }
}