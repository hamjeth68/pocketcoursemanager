package com.example.pocketcoursemanager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import java.util.ArrayList;
import java.util.List;

public  class login extends AppCompatActivity {
    Spinner spinner1;
    EditText txtus, txtps;
    Button btnlogin, btnsignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        spinner1 = findViewById(R.id.spinner1);
        txtus = findViewById(R.id.txtus);
        txtps = findViewById(R.id.txtps);
        btnlogin = findViewById(R.id.btnlogin);
        btnsignup = findViewById(R.id.btnsignup);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.selectlogintype, R.layout.support_simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = spinner1.getSelectedItem().toString();


                if (item.equals("Admin")) {
                    Intent intent = new Intent(login.this, mainscreen.class);
                    startActivity(intent);

                } else if (item.equals("Students")) {
                    Intent intent = new Intent(login.this, mainscreen.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();

                }
                //students
                btnlogin = findViewById(R.id.btnlogin);
                btnlogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String item = spinner1.getSelectedItem().toString();
                        //Toast.makeText(login.this, item, Toast.LENGTH_SHORT).show();

                        if (item.equals("Students")) {
                            Intent intent = new Intent(login.this, cwstudent.class);
                            startActivity(intent);

                        } else if (item.equals("Admin")) {
                            Intent intent = new Intent(login.this, cwadmin.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();

                        }
                    }

                    ;
                });


            }

            ;


        });



    }
}