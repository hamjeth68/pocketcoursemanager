package com.example.pocketcoursemanager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView lg,cd1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lg=(ImageView)findViewById(R.id.lg);
        cd1=(ImageView)findViewById(R.id.cd1);

        getSupportActionBar().hide();


        lg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,login.class);
                startActivity(i);

                cd1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent b = new Intent(MainActivity.this,Descri.class);
                        startActivity(b);
                    }
                });
            }
        });

    }
}